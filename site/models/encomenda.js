const db = require("./db")


const Encomenda = db.sequelize.define("Encomenda", {
    data: {
        primaryKey: true,
        type: db.Sequelize.DATEONLY
    },
    idCliente: {
        type: db.Sequelize.INTEGER,        
        references: {
            model: "Clientes",
            key: "id"
        }
    },
    idProduto: {
        type: db.Sequelize.INTEGER,
        references: {
            model: "Produtos",
            key: "id"
        }
    }
});


module.exports = Encomenda;