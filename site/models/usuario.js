const db = require("./db")

const Usuario = db.sequelize.define("Usuario", {
    nome: db.Sequelize.STRING,
    funcao: db.Sequelize.ENUM("Gerente", "Caixa"),
    cpf: {
        type: db.Sequelize.CHAR(15),
        unique: true
    },
    senha: db.Sequelize.STRING
});

module.exports = Usuario;