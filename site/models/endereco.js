const db = require("./db")

const Endereco = db.sequelize.define("Endereco", {
    rua: db.Sequelize.STRING,
    bairro: db.Sequelize.STRING,
    numero: db.Sequelize.STRING(4),
    idCliente: {
        type: db.Sequelize.INTEGER,        
        references: {
            model: "Clientes",
            key: "id"
        }
    }
});

module.exports = Endereco;