const db = require("./db")

const Venda = db.sequelize.define("Venda", {
    dataVenda: db.Sequelize.DATEONLY, 
    tipoPagamento: db.Sequelize.ENUM("Credito", "Debito", "Dinheiro", "Fiado"),
    desconto: db.Sequelize.DOUBLE(5,2),
    valorTotal: db.Sequelize.DOUBLE(5,2),
    cpfCliente: db.Sequelize.CHAR(15),
    idUsuario: {
        type: db.Sequelize.INTEGER,
        references: {
            model: "Usuarios",
            key: "id"
        }
    }
  
});

module.exports = Venda;