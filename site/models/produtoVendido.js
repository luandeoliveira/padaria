const db = require("./db")

const ProdutoVendido = db.sequelize.define("ProdutosVendidos", {
  
    idVenda: {
        type: db.Sequelize.INTEGER,
        references: {
            model: "Vendas",
            key: "id"
        }
    },
    codProduto: db.Sequelize.BIGINT, 
    precoVenda: db.Sequelize.DOUBLE(5,2),
    qtdVendida: db.Sequelize.SMALLINT

});

module.exports = ProdutoVendido;