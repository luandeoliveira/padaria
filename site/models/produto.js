const db = require("./db")

const Produto = db.sequelize.define("Produto", {
    codBarra: db.Sequelize.BIGINT,
    nome: db.Sequelize.STRING,
    validade: db.Sequelize.DATEONLY,
    quantidadeTotal: db.Sequelize.SMALLINT,
    quantidadeMinima: db.Sequelize.SMALLINT,
    precoVenda: db.Sequelize.DOUBLE(5,2),
    idCategoria: {
        type: db.Sequelize.INTEGER,
        references: {
            model: "Categoria",
            key: "id"
        }
    }
});

module.exports = Produto;