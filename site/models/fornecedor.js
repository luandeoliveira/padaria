const db = require("./db")

const Fornecedor = db.sequelize.define("Fornecedor", {
    nome: db.Sequelize.STRING, 
    telefone: db.Sequelize.STRING,
    cnpj: {
        type: db.Sequelize.STRING(14),
        unique: true           
    }
});


module.exports = Fornecedor;