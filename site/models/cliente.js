const db = require("./db")

const Cliente = db.sequelize.define("Cliente", {
    nome: db.Sequelize.STRING, 
    telefone: db.Sequelize.STRING,
    cpf: {
        type: db.Sequelize.CHAR(15),
        unique: true           
    },
    rua: db.Sequelize.STRING,
    bairro: db.Sequelize.STRING,
    numero: db.Sequelize.SMALLINT
});

module.exports = Cliente;