const db = require("./db")

const Categoria = db.sequelize.define("Categoria", {
    nome: db.Sequelize.STRING       
});

module.exports = Categoria;