DELIMITER //
DROP TRIGGER IF EXISTS t_AtualizarQtdProduto; 
CREATE TRIGGER t_AtualizarQtdProduto AFTER INSERT ON ProdutosVendidos FOR EACH ROW 
BEGIN        
    UPDATE Produtos 
    SET quantidadeTotal = quantidadeTotal - NEW.qtdVendida;     
END//
DELIMITER ; 

