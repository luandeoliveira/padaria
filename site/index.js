const express = require("express");
const app = express();
const handlebars = require("express-handlebars");
const bodyParser = require("body-parser");
const usuario = require("./models/usuario");
const cliente = require("./models/cliente");
const fornecedor = require("./models/fornecedor");
const produto = require("./models/produto");
const venda = require("./models/venda");
const db = require("./models/db");
const produtosVendidos = require("./models/produtoVendido");


//Config 
    // Template Engine
        app.engine("handlebars", handlebars({defaultLayout: "main"}))
        app.set("view engine", "handlebars")
    //Body Parser
        app.use(bodyParser.urlencoded({extended: false}))
        app.use(bodyParser.json())    
    
// **ROTAS** // 
app.get("/", function(req, res){
    res.render("telaPrincipal")
});

//Login 

app.get("/login", function(req, res){
    usuario.findAll().then(function (usuarios){
        res.render('signIn', { title: 'Login', layout: 'login', usuarios: usuarios, error: "null"});
    });
});   
  
//Validação

app.post("/login", function(req, res){
    var data = req.body;
    console.log(data);
    usuario.count({where: {'cpf': data.cpf}}).then(function(users){
        if(users != 0){            
            usuario.count({where: {'senha': data.senha}}).then(function(senha){
                if(senha != 0){
                    res.redirect("/");
                }
                else{
                    res.render('signIn', { title: 'Login', layout: 'login', error: "senha"});
                }

            });
        }
        else{
            res.render('signIn', { title: 'Login', layout: 'login', error: "login"});
        }
    });

});


// ROTAS USUÁRIO //

app.get("/addUsuarios", function(req, res){
    res.render("addUsuarios")
});

app.post("/addUsuario", function(req, res){
    usuario.create({
        nome: req.body.nomeUsuario,        
        funcao: req.body.funcaoUsuario, 
        cpf: req.body.cpfUsuario,
        senha: req.body.senha
    }).then(function(){
        res.redirect("/gerenciarUsuarios")
    }).catch(function(erro){
        res.send("Erro! "+erro)
    })  

    
});

app.get("/gerenciarUsuarios", function(req, res){    
    usuario.findAll().then(function (usuarios){
        res.render("gerenciarUsuarios", {usuarios: usuarios});
    });   

});

app.get("/deletarUsuario/:id", function(req, res){
    usuario.destroy({where: {'id': req.params.id}}).then(function (){
        res.redirect("/gerenciarUsuarios")
    }).catch(function (erro){
        res.send("Erro")
    })
});

app.post("/editarUsuario/:id", function(req, res){
    usuario.update({nome: req.body.nomeUsuarioEdit,
                    cpf: req.body.cpfUsuarioEdit,
                    funcao: req.body.funcaoUsuarioEdit,
                    senha: req.body.senhaEdit}, 
        {where: {'id': req.params.id}}).then(function (){
        res.redirect("/gerenciarUsuarios")
    }).catch(function (erro){
        res.send("Erro: "+erro)
    })
});

// ROTAS CLIENTES // 

app.get("/addClientes", function(req, res){
    res.render("addClientes")
});

app.post("/addClientes", function(req, res){
    cliente.create({
        nome: req.body.nomeCliente, 
        telefone: req.body.telefoneCliente,    
        cpf: req.body.cpfCliente,  
        bairro: req.body.bairroCliente, 
        rua: req.body.ruaCliente, 
        numero: req.body.numeroCliente      
    }).then(function(){        
        res.redirect("/gerenciarClientes")
    }).catch(function(erro){
        res.send("Erro! "+erro)
    })  

    
});

app.get("/gerenciarClientes", function(req, res){    
    cliente.findAll().then(function (clientes){
        res.render("gerenciarClientes", {clientes: clientes});
    }); 
   

});

app.get("/deletarCliente/:id", function(req, res){
    cliente.destroy({where: {'id': req.params.id}}).then(function (){
        res.redirect("/gerenciarClientes")
    }).catch(function (erro){
        res.send("Erro")
    })
});

app.post("/editarCliente/:id", function(req, res){
    cliente.update({nome: req.body.nomeClienteEdit,
                   telefone: req.body.telefoneClienteEdit,
                    cpf: req.body.cpfClienteEdit,
                    rua: req.body.ruaClienteEdit,
                    bairro: req.body.bairroClienteEdit,
                    numero: req.body.numeroClienteEdit}, 
        {where: {'id': req.params.id}}).then(function (){
            res.redirect("/gerenciarClientes")
    }).catch(function (erro){
        res.send("Erro: "+erro)
    })
});

// ROTAS FORNECEDORES 

app.get("/addFornecedores", function(req, res){
    res.render("addFornecedores")
});

app.post("/addFornecedores", function(req, res){
    fornecedor.create({
        nome: req.body.nomeFornecedor, 
        telefone: req.body.telefoneFornecedor,    
        cnpj: req.body.cnpjFornecedor            
    }).then(function(){        
        res.redirect("/gerenciarFornecedores")
    }).catch(function(erro){
        res.send("Erro! "+erro)
    })  

    
});

app.get("/gerenciarFornecedores", function(req, res){    
    fornecedor.findAll().then(function (fornecedores){
        res.render("gerenciarFornecedores", {fornecedores: fornecedores});
    }); 
   

});

app.get("/deletarFornecedor/:id", function(req, res){
    fornecedor.destroy({where: {'id': req.params.id}}).then(function (){
        res.redirect("/gerenciarFornecedores")
    }).catch(function (erro){
        res.send("Erro")
    })
});

app.post("/editarFornecedor/:id", function(req, res){
    fornecedor.update({nome: req.body.nomeFornecedorEdit,
                    telefone: req.body.telefoneFornecedorEdit,
                    cnpj: req.body.cnpjFornecedorEdit},                  
        {where: {'id': req.params.id}}).then(function (){
            res.redirect("/gerenciarFornecedores")
    }).catch(function (erro){
        res.send("Erro: "+erro)
    })
});


// ROTAS VENDAS 

app.get("/novaVenda", function(req, res){
    produto.findAll().then(function (produtos){
        res.render("addVenda", {produtos: produtos});
    });    
});

app.post("/novaVenda", function(req, res){
    var data = req.body;
    console.log(data) 
   
    venda.create({
        dataVenda: data.venda.dataVenda, 
        tipoPagamento: data.venda.tipoPagamento,
        desconto: data.venda.desconto, 
        valorTotal: data.venda.valorTotal,
        idUsuario: data.venda.idUsuario,
        cpfCliente: data.venda.cpfCliente

    }).then(function(){
        db.sequelize.query("SELECT id FROM Vendas ORDER BY createdAt DESC LIMIT 1", {
            type: db.sequelize.QueryTypes.SELECT
        }).then(function(codVenda){        
            data.produtos.forEach(function (produto){
                produtosVendidos.create({
                    codProduto: produto.cod,
                    idVenda: codVenda[0].id, 
                    precoVenda: produto.valor, 
                    qtdVendida: produto.qtd
                });
            });       
            
                
        });
    })

    
    
});

app.get("/gerenciarVendas", function(req, res){    
    venda.findAll().then(function (vendas){     
        produtosVendidos.findAll().then(function (pv){
            res.render("gerenciarVendas", {venda: vendas, produtosVendidos: pv});
        }) 
       
             
    }); 
   
});



// ROTAS PRODUTOS 

app.get("/addProdutos", function(req, res){
    res.render("addProduto");
});

app.post("/addProduto", function(req, res){
    produto.create({
        codBarra: req.body.codBarra,
        nome: req.body.nomeProduto,
        validade: req.body.validadeProduto,
        quantidadeTotal: req.body.quantidadeProduto,
        quantidadeMinima: req.body.quantidadeMinimaProduto,
        precoVenda: req.body.precoProduto,
        idCategoria: req.body.categoriaSelect
    }).then(function(){        
        res.redirect("/gerenciarProdutos")
    }).catch(function(erro){
        res.send("Erro! "+erro)
    })  
});

app.get("/gerenciarProdutos", function(req, res){    
    produto.findAll().then(function (produto){     
        res.render("gerenciarProduto", {produto: produto});      
             
    }); 
   
});

app.get("/deletarProduto/:id", function(req, res){
    produto.destroy({where: {'id': req.params.id}}).then(function (){
        res.redirect("/gerenciarProdutos")
    }).catch(function (erro){
        res.send("Erro")
    })
});

app.post("/editarProduto/:id", function(req, res){
    console.log("Editando produto");
    produto.update({codBarra: req.body.codBarraEdit,
                    nome: req.body.nomeProdutoEdit,
                    validade: req.body.validadeProdutoEdit,
                    quantidadeTotal: req.body.quantidadeProdutoEdit,
                    quantidadeMinima: req.body.quantidadeMinimaProdutoEdit,
                    precoVenda: req.body.precoProdutoEdit,
                    idCategoria: req.body.categoriaSelectEdit},               
        {where: {'id': req.params.id}}).then(function (){
            res.redirect("/gerenciarProdutos")
    }).catch(function (erro){
        res.send("Erro: "+erro)
    })
});

// Localizações estáticas // 

app.use(express.static(__dirname + "/static"));

// Iniciando servidor 

app.listen(8081, function(){
    console.log("Servidor rodando!")
});

