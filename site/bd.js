const Sequelize = require("sequelize")
const sequelize = new Sequelize("destak", "luan", "12345", {
    host: "localhost",
    dialect: "mysql"
});

const Categoria = sequelize.define("Categoria", {
    nome: Sequelize.STRING       
});

const Produto = sequelize.define("Produto", {
    codBarra: Sequelize.BIGINT,
    nome: Sequelize.STRING,
    validade: Sequelize.DATEONLY,
    quantidadeTotal: Sequelize.SMALLINT,
    quantidadeMinima: Sequelize.SMALLINT,
    precoVenda: Sequelize.DOUBLE(5,2),
    idCategoria: {
        type: Sequelize.INTEGER,
        references: {
            model: "Categoria",
            key: "id"
        }
    }
});

const Usuario = sequelize.define("Usuario", {
    nome: Sequelize.STRING,
    funcao: Sequelize.ENUM("Gerente", "Caixa"),
    cpf: {
        type: Sequelize.CHAR(15),
        unique: true
    },
    senha: Sequelize.STRING
});

const Cliente = sequelize.define("Cliente", {
    nome: Sequelize.STRING, 
    telefone: Sequelize.STRING,
    cpf: {
        type: Sequelize.CHAR(15),
        unique: true           
    },
    rua: Sequelize.STRING,
    bairro: Sequelize.STRING,
    numero: Sequelize.SMALLINT
});

const Venda = sequelize.define("Venda", {
    dataVenda: Sequelize.DATEONLY, 
    tipoPagamento: Sequelize.ENUM("Credito", "Debito", "Dinheiro", "Fiado"),
    desconto: Sequelize.DOUBLE(5,2),
    valorTotal: Sequelize.DOUBLE(5,2),
    idUsuario: {
        type: Sequelize.INTEGER,
        references: {
            model: "Usuarios",
            key: "id"
        }
    },
    cpfCliente: Sequelize.CHAR(15)       
    
});

const Encomenda = sequelize.define("Encomenda", {
    data: {
        primaryKey: true,
        type: Sequelize.DATEONLY
    },
    idCliente: {
        type: Sequelize.INTEGER,        
        references: {
            model: "Clientes",
            key: "id"
        }
    },
    idProduto: {
        type: Sequelize.INTEGER,
        references: {
            model: "Produtos",
            key: "id"
        }
    }
});

const Fornecedor = sequelize.define("Fornecedor", {
    nome: Sequelize.STRING, 
    telefone: Sequelize.STRING,
    cnpj: {
        type: Sequelize.STRING(14),
        unique: true           
    }
});

const ProdutoVendido = sequelize.define("ProdutosVendido", {
    idVenda: {
        type: Sequelize.INTEGER,
        references: {
            model: "Vendas",
            key: "id"
        }
    },
    codProduto: Sequelize.BIGINT, 
    precoVenda: Sequelize.DOUBLE(5,2),
    qtdVendida: Sequelize.SMALLINT

});

/*

const Endereco = sequelize.define("Endereco", {
    rua: Sequelize.STRING,
    bairro: Sequelize.STRING,
    numero: Sequelize.STRING(4),
    idCliente: {
        type: Sequelize.INTEGER,        
        references: {
            model: "Clientes",
            key: "id"
        }
    }
});
*/

//sequelize.sync()





Usuario.create({
    nome: "Josivaldo",
    funcao: "Gerente",
    cpf: "2323232",
    senha: "34343434"
});










